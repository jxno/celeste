Celeste = { }
Celeste.prefix = "."
Celeste.vers = '1.0.1'

Celeste.commands = {
	["help!"]  	 = function(...) require('commands.help')(...) end,
	["ping!"]  	 = function(...) require('commands.ping')(...) end,
	["compile!"]	= function(...) require('commands.compile')(...) end,
	["echo!"]		 = function(...) require('commands.echo')(...) end
}

-- table of different languages
  Celeste.langfunctions = {
		["elixir"] = 'commands.langs.elixir',
    ["lua"]    = 'commands.langs.lua',
    ["moon"]   = 'commands.langs.moon',
    ["c++"]    = 'commands.langs.c++',
    ["python"] = 'commands.langs.python'
  }

function Celeste:registerCommand(msg)
	local cmd = {}
	if msg == nil then return end
	for str in string.gmatch(msg, "([^".."%s".."]+)") do
		table.insert(cmd, str)
	end
	table.remove(cmd, 1)

	local str = ""
	for i = 1,#cmd do
		str = str..cmd[i].." "
	end
	return cmd, str
end

--  ? Return everything before a space, remove first instance of the prefix
	    --* Strip first word+whitespace (the command)
	    --* For each word separated by a space, place it into a numerically ordered table
    		-- !command arg1 arg2 argn
    		--* 1: arg1
    		--* 2: arg2
    		--* n: argn

return Celeste
