return function(...)
	t = select(1, ...)
	local filename = 'commands/cache/code' .. math.floor(os.clock() * 420.69) .. ".moon"
	local code = io.open(filename, 'w+')
	for i = 2, #t do
		code:write(t[i], "\n")
	end
	code:close()
	function os.capture(input)
	  local f = assert(io.popen(input, 'r'))
	  return assert(f:read('*a'))
	end
	print('running moonscript code now!!')
	return os.capture("moon "..filename)
end