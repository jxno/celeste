return function(...)
	t = select(1, ...)
	local filename = 'commands/cache/code' .. math.floor(os.clock() * 420.69) .. ".lua"
	local code = io.open(filename, 'w+')
	for i = 2, #t do
		code:write(t[i], "\n")
	end
	code:close()
	function os.capture(input)
	  local f = assert(io.popen(input, 'r'))
	  return assert(f:read('*a'))
	end
	print('running lua code now!!')
	return os.capture("lua "..filename)
end