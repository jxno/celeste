return function(...)
	t = select(1, ...)
	msg = select(2, ...)
	local filename = 'commands/cache/code' .. math.floor(os.clock() * 420.69) .. ".cc"
	local code = io.open(filename, 'w+')
	for i = 2, #t do
		if string.match(t[i], "cin") then 	-- we cant let users await input otherwise celeste will hang
			msg:reply('cant await input otherwise ill crash!!')
			return
		end
		code:write(t[i], "\n")
	end
	code:close()
	function os.capture(input)
	  local f = assert(io.popen(input, 'r'))
	  return assert(f:read('*a'))
	end
	print('running c++ code now!!')
	return os.capture(
			"clang++ -lstdc++ -std=c++17 " .. filename .. " -o ".. filename .. '+ ' -- compile
			.. "&& " .. "./" .. filename .. "+" -- run
		)
end