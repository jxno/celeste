local json = require("deps.rxi-json-lua")
local inspect = require("deps.inspect")
local dataset = { }
local dict = { }
local isTrained = false
local attemptTrain = function() end
return function(...)
  if select(2, ...)[1] == nil then
    select(1, ...).channel:send("train/post")
    return
  end
  local _exp_0 = select(2, ...)[1]
  if "train" == _exp_0 then
    select(1, ...).channel:send("Training!")
    select(1, ...).channel:send("Parsing data.json")
    local f = assert(io.open("data.json", "rb"))
    local content = f:read("*all")
    f:close()
    content = json.decode(content)
    select(1, ...).channel:send("Attempting to train...")
    for _, line in ipairs(content) do
      local s = {
        "START_TOKEN"
      }
      for word in line:gmatch("[^ ]+") do
        s[#s + 1] = word
      end
      for key, word in ipairs(s) do
        local nextWord = ""
        local z = s[key + 1]
        if z == nil then
          nextWord = "END_TOKEN"
        else
          nextWord = tostring(s[key + 1])
        end
        if not dataset[word] then
          dataset[word] = { }
          if nextWord ~= nil then
            dataset[word][nextWord] = 1
          end
        else
          if nextWord ~= nil then
            if dataset[word][nextWord] then
              local _update_0, _update_1 = word, nextWord
              dataset[_update_0][_update_1] = dataset[_update_0][_update_1] + 1
            else
              dataset[word][nextWord] = 1
            end
          end
        end
      end
    end
    isTrained = true
    select(1, ...).channel:send("Training complete.")
    return
  elseif "post" == _exp_0 then
    if isTrained then
      local myStr = { }
      local currentWord = "START_TOKEN"
      local mod = 15
      while currentWord ~= "END_TOKEN" do
        local map = { }
        for k, v in pairs(dataset[currentWord]) do
          for i = 1, tonumber(v) do
            table.insert(map, k)
          end
        end
        table.insert(myStr, map[math.random(#map)])
        if mod > 0 then
          if myStr[#myStr] == "END_TOKEN" then
            mod = mod - 1
            table.remove(myStr, #myStr)
          end
        end
        currentWord = tostring(myStr[#myStr])
      end
      local s = ""
      table.remove(myStr, #myStr)
      for _, v in pairs(myStr) do
        s = s .. " " .. v
      end
      return select(1, ...).channel:send(s)
    else
      return select(1, ...).channel:send("Cass not trained!")
    end
  end
end
